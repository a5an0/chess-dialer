/*

This program finds the number of phone numbers that a chess piece can generate by
making its legal moves on a phone dialpad like this one:
1 2 3
4 5 6
7 8 9
  0

The `PhoneList` type implements `Iterator`, and each iteration generates a valid
phone number. That means that you can generate as many or as few as you want.
To get the count, we have to generate all of them, but if you just wanted to get
a sample, `take`ing numbers from the iterator will be much more efficient.
There is a commented out example of this in `main()`.

When you run this program, it will create a thread for each starting position.
Each thread will figure out how many phone numbers can be generated for its
position and will send the count back to the main thread. The main thread sorts
all the results and displays them.

 */

#[derive(std::clone::Clone)]
enum Piece {
    Rook,
    // Knight,
    // Bishop,
    // Queen,
    // King,
}

fn get_move_map(piece: &Piece) -> Vec<Vec<usize>> {
    match piece {
        Piece::Rook => vec![
            vec![2, 5, 8],
            vec![2, 3, 4, 7],
            vec![1, 3, 5, 8, 0],
            vec![1, 2, 6, 9],
            vec![5, 6, 7, 1],
            vec![2, 4, 6, 8, 0],
            vec![3, 4, 5, 9],
            vec![1, 4, 8, 9],
            vec![2, 5, 7, 9, 0],
            vec![3, 6, 7, 8],
        ],
        // Piece::Knight => vec![],
        // Piece::Bishop => vec![],
        // Piece::Queen => vec![],
        // Piece::King => vec![],
    }
}

type PhoneNumber = Vec<usize>;

#[derive(std::clone::Clone, Debug)]
struct Cursor {
    didx: usize,
    dvar: usize,
}

#[derive(std::clone::Clone)]
struct PhoneList {
    piece: Piece,
    start_loc: usize,
    curr_num: PhoneNumber,
    stack: Vec<Cursor>,
    move_map: Vec<Vec<usize>>,
}

impl PhoneList {
    fn new(p: Piece, start_loc: usize) -> Self {
        PhoneList {
            move_map: get_move_map(&p),
            piece: p,
            start_loc,
            curr_num: Vec::new(),
            stack: Vec::new(),
        }
    }

    fn generate_number(&mut self) -> Option<PhoneNumber> {
        if self.stack.len() == 0 && self.curr_num.len() == 0 {
            self.stack.push(Cursor {
                didx: self.start_loc,
                dvar: 0,
            });
        }
        loop {
            let frame = self.stack.pop().unwrap();
            match self.move_map.get(frame.didx).unwrap().get(frame.dvar) {
                Some(idx) => {
                    self.stack.push(Cursor {
                        didx: frame.didx,
                        dvar: frame.dvar + 1,
                    });
                    self.curr_num.push(*idx);
                    if self.curr_num.len() == 10 {
                        let n = self.curr_num.clone();
                        let _ = self.curr_num.pop();
                        return Some(n);
                    }
                    self.stack.push(Cursor {
                        didx: *idx,
                        dvar: 0,
                    })
                }
                None => {
                    let _ = self.curr_num.pop();
                }
            };
            if self.stack.len() == 0 {
                return None;
            }
        }
    }
}

impl Iterator for PhoneList {
    type Item = PhoneNumber;

    fn next(&mut self) -> Option<Self::Item> {
        self.generate_number()
    }
}

fn main() {
    // let start_loc = 5;
    // let generator = PhoneList::new(Piece::Rook, start_loc);
    // println!(
    //     "A rook can generate {} numbers starting at position {}",
    //     generator.count(),
    //     start_loc
    // );
    // let n = 20;
    // println!("Here are the first {} of them:", n);
    // let generator = PhoneList::new(Piece::Rook, start_loc);
    // let numbers = generator.take(n);
    // for n in numbers {
    //     let sn: Vec<String> = n.iter().map(|x| x.to_string()).collect();
    //     let area_code = format!("{}", &sn[0..3].join(""));
    //     let exchange = format!("{}", &sn[3..6].join(""));
    //     let number = format!("{}", &sn[6..10].join(""));

    //     println!("({}) {}-{}", area_code, exchange, number);
    // }

    use std::sync::mpsc::channel;
    use std::thread;

    let (tx, rx) = channel();
    for n in 0..10 {
        let tx = tx.clone();
        thread::spawn(move || {
            let generator = PhoneList::new(Piece::Rook, n);
            let count = generator.count();
            tx.send((n, count)).unwrap();
        });
    }
    let mut results = Vec::new();
    for _i in 0..10 {
        results.push(rx.recv().unwrap());
    }
    let sorted = results.as_mut_slice();
    sorted.sort_by(|a, b| a.cmp(b));
    for (n, count) in sorted {
        println!(
            "Starting at number {}, a Rook can generate {} phone numbers",
            n, count
        );
    }
}
